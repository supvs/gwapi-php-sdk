
## Supgw

gwapi-php-sdk

## 安装

* 通过composer，这是推荐的方式，可以使用composer.json 声明依赖，或者运行下面的命令。
```bash
$ composer require supvs/gwapi-php-sdk
```
* 直接下载安装，SDK 没有依赖其他第三方库，但需要参照 composer的autoloader，增加一个自己的autoloader程序。

## 运行环境

    php: >=7.4

## 使用方法

```php    
    use Supgw\Api\SupplyClient;
    //appkey、appSecret 在开放平台获取
    $apiUrl = 'API URL';
    $appKey = "your appkey"; 
    $appSecret = "your appSecret";
    
    try {
    	$supplyClient = new SupplyClient($apiUrl,$appKey,$appSecret);
    } catch (OssException $e) {
    	printf(__FUNCTION__ . "creating supplyClient instance: FAILED\n");
    	printf($e->getMessage() . "\n");
    	return null;
    }
    
    //获取用户资料
    $param = [];//请求参数
    $method = 'get';//请求方法
    $action = 'api/user';//请求资源名
    $response = $supplyClient->getApiResponse($method,$action,$param);
```    
